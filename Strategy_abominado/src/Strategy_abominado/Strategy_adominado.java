package Strategy_abominado;

interface Strategy_adominado {
	public void executeAlgorithm();
}

class ConcreteStrategyA implements Strategy_adominado {
	public void executeAlgorithm() {
		System.out.println("Concrete Strategy A");
	}
}

class ConcreteStrategyB implements Strategy_adominado {
	public void executeAlgorithm() {
		System.out.println("Concrete Strategy B");
	}
}

class ConcreteStrategyC implements Strategy_adominado {
	public void executeAlgorithm() {
		System.out.println("Concrete Strategy C");
	}
}
 