package Strategy_abominado;

public class Context_adominado {
	private Strategy_adominado strategy1 = new ConcreteStrategyA();
	private Strategy_adominado strategy2= new ConcreteStrategyB();
	private Strategy_adominado strategy3 = new ConcreteStrategyC();

	public void execute1() {
		strategy1.executeAlgorithm();
	}

	public void setStrategy1(Strategy_adominado strategy) {
		strategy1 = strategy1;
	}

	public Strategy_adominado getStrategy1() {
		return strategy1;
	}
	
	//Strategy 2
	public void execute2() {
		strategy2.executeAlgorithm();
	}

	public void setStrategy2(Strategy_adominado strategy) {
		strategy2 = strategy2;
	}

	public Strategy_adominado getStrategy2() {
		return strategy2;
	}

	//Strategy 3
	public void execute3() {
		strategy3.executeAlgorithm();
	}

	public void setStrategy3(Strategy_adominado strategy) {
		strategy3 = strategy3;
	}

	public Strategy_adominado getStrategy3() {
		return strategy3;
	}

}
