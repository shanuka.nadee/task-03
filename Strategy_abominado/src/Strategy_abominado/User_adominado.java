package Strategy_abominado;

public class User_adominado {

	public static void main(String[] args) {
		Context_adominado context1 = new Context_adominado();
		context1.execute1();
		context1.setStrategy1(new ConcreteStrategyA());
		
		
		Context_adominado context2 = new Context_adominado();
		context2.execute2();
		context2.setStrategy2(new ConcreteStrategyB());

		
		Context_adominado context3 = new Context_adominado();
		context3.execute3();
		context3.setStrategy3(new ConcreteStrategyC());
		
	}
}